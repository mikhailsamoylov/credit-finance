var validate = function () {
    var comment_length = $('#comment').val().length;
    if (comment_length == 0) {
        alert('Comment field should not be empty');
        return false;
    }
    if (comment_length > 200) {
        alert('Length of a comment should not be bigger than 200 characters');
        return false;
    }
    var phone_number = $('#phone_number').val();
    if ((phone_number.length != 0) && !/^\+\d{11,13}$/.test(phone_number)) {
        alert('Phone number is not valid');
        return false;
    }
    if (($('#address').val().length > 200)) {
        alert('Length of an address should not be bigger than 200 characters');
        return false;
    }
    return true;
};

$('#results').submit(function (event) {
    if (!validate()) {
        event.preventDefault();
    }
});
