<?php

class CallController extends CController
{
    public function actionInbcall()
    {
        /** @var CApplication $app */
        $app = Yii::app();
        $db = new DB();
        try {
            $phone = $app->db->quoteValue(substr($app->request->getParam('phone'), 5));
            if ($db->query("SELECT cust_id, id FROM customer_phone WHERE phone_number = $phone", false)) {
                $phone_info = $db->fetch();
                if (is_null($phone_info)) {
                    throw new CException('A person with specified phone number does not exist in the database');
                }
                $customer_id = $phone_info['cust_id'];
                $phone_id = $phone_info['id'];
            } else {
                throw new DBException($db->getDescription());
            }

            $customer_info_query = <<<QUERY
SELECT
	(cst.cust_surname || ' ' || cst.cust_name || ' ' || cst.cust_patroname) FIO,
	cst.cust_birthday,
	cst_acc.account_id,
	cst_acc.account_balance,
	cst_phn.phone_number,
	cst_add.address
FROM
	customer cst
JOIN customer_account cst_acc ON cst_acc.cust_id = cst.id
JOIN customer_phone cst_phn ON cst_phn.cust_id = cst.id
JOIN customer_address cst_add ON cst_add.cust_id = cst.id
WHERE cst.id = $customer_id;
QUERY;

            if ($db->query($customer_info_query)) {
                $customer_info = $db->fetchAll();
            } else {
                throw new DBException($db->getDescription());
            }

            $FIO = $customer_info[0]['FIO'];
            $birthday = $customer_info[0]['cust_birthday'];

            $accounts = array();
            foreach ($customer_info as $row) {
                $accounts[$row['account_id']] = $row['account_balance'];
            }
            $account_sum = array_sum($accounts);

            $phone_numbers = array_unique(
                array_map(
                    function ($row) {
                        return $row['phone_number'];
                    }, $customer_info
                )
            );

            $addresses = array_unique(
                array_map(
                    function ($row) {
                        return $row['address'];
                    }, $customer_info
                )
            );
            $this->render('customer_info', array(
                'FIO' => $FIO,
                'birthday' => $birthday,
                'account_sum' => $account_sum,
                'phone_numbers' => $phone_numbers,
                'addresses' => $addresses,
                'phone_id' => $phone_id
            ));
        } catch (CException $e) {
            $this->render('error', array('data' => $e->getMessage()));
            $app->end();
        }
    }

    public function actionDialog()
    {
        $db = new DB();
        $db->query('SELECT * FROM ref_results');
        $results = $db->fetchAll();
        $db->query('SELECT * FROM ref_phone_type');
        $phone_types = $db->fetchAll();
        $db->query('SELECT * FROM ref_address_type');
        $address_types = $db->fetchAll();
        $this->render('dialog', array(
                'results' => $results,
                'phone_types' => $phone_types,
                'address_types' => $address_types,
                'phone_id' => Yii::app()->request->getParam('phone_id'))
        );
    }
}
