<?php

class SaveController extends CController
{
    public function actionIndex()
    {
        /** @var CApplication $app */
        $app = Yii::app();
        $db = new DB();
        /** @var CDbTransaction $transaction */
        $transaction = $db->beginTransaction();
        try {
            $phone_id = $app->request->getParam('phone_id');
            if ($db->query("SELECT cust_id FROM customer_phone WHERE id = $phone_id", false)) {
                $customer_id = $db->fetch()['cust_id'];
            } else {
                throw new DBException($db->getDescription());
            }
            $result_id = $app->request->getParam('result');
            $comment_text = $app->db->quoteValue($app->request->getParam('comment'));

            $insert_query = <<<QUERY
INSERT INTO
    contact_history (cust_id, phone_id, result_id, comment_text)
VALUES
    ($customer_id, $phone_id, $result_id, $comment_text);
QUERY;
            if (!$db->query($insert_query)) {
                throw new DBException($db->getDescription());
            }

            $phone_number = $app->request->getParam('phone_number');
            if ($phone_number) {
                $phone_type = $app->request->getParam('phone_type');
                $insert_query = <<<QUERY
INSERT INTO
    customer_phone (cust_id, phone_number, phone_type)
VALUES
    ($customer_id, $phone_number, $phone_type);
QUERY;
                if (!$db->query($insert_query)) {
                    throw new DBException($db->getDescription());
                }
            }

            $address = $app->db->quoteValue($app->request->getParam('address'));
            if ($address != "''") {
                $address_type = $app->request->getParam('address_type');
                $insert_query = <<<QUERY
INSERT INTO
    customer_address (cust_id, address, address_type)
VALUES
    ($customer_id, $address, $address_type);
QUERY;
                if ($address && !$db->query($insert_query)) {
                    throw new DBException($db->getDescription());
                }
            }

            $this->render('hold');
            $transaction->commit();
        } catch (CException $e) {
            $transaction->rollback();
            $this->render('error', array('data' => $e->getMessage()));
            $app->end();
        }
    }
}
