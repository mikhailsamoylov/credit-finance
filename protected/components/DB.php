<?php

class DB implements IDB
{
    /**
     * @var CDbConnection $db - Yii database class
     */
    private $db;
    /**
     * @var boolean $auto_fetch_all
     */
    private $auto_fetch_all;
    /**
     * @var array $data - query result array
     */
    private $data;
    /**
     * @var string $description - description of an error
     */
    private $description;
    /**
     * @var integer $current_row_pointer - pointer at current row in a result
     */
    private $current_row_pointer = 0;

    function __construct($db = null)
    {
        $this->db = is_null($db) ? Yii::app()->db : new CDbConnection($db);
    }

    /**
     * @param $sql - request string
     * @param $auto_fetch_all
     * @return bool - выполняет запрос и возвращает true при успехе, false при ошибке
     */
    public function query($sql, $auto_fetch_all = true)
    {
        $this->current_row_pointer = 0;
        $this->auto_fetch_all = $auto_fetch_all;
        try {
            $this->data = $this->db->createCommand($sql)->queryAll();
        } catch (Exception $e) {
            $this->description = $e->getMessage();
            return false;
        }
        return true;
    }

    /* @return array - возвращает двумерный массив результатов SELECT */
    public function fetchAll()
    {
        return $this->data;
    }

    /* @return array - возвращает fetch очередной записи, если $auto_fetch_all = false */
    public function fetch()
    {
        if ($this->current_row_pointer >= count($this->data)) {
            return null;
        }
        return !$this->auto_fetch_all ? $this->data[$this->current_row_pointer++] : false;
    }

    /* @return string - возвращает текст ошибки */
    public function getDescription()
    {
        return $this->description;
    }

    public function beginTransaction()
    {
        return $this->db->beginTransaction();
    }
}
