<?php

interface IDB
{
    /* подключение к БД $db */
    function __construct($db = null);

    /**
     * @param $sql - request string
     * @param $auto_fetch_all
     * @return bool - выполняет запрос и возвращает true при успехе, false при ошибке
     */
    public function query($sql, $auto_fetch_all = true);

    /* @return array - возвращает двумерный массив результатов SELECT */
    public function fetchAll();

    /* @return array - возвращает fetch очередной записи, если $auto_fetch_all = false */
    public function fetch();

    /* @return string - возвращает текст ошибки */
    public function getDescription();
}
