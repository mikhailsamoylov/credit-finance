<?php
/**
 * @var string $FIO
 * @var string $birthday
 * @var string $account_sum
 * @var array $phone_numbers
 * @var array $addresses
 * @var string $phone_id
 *
 */
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <title>Call to Credit Finance</title>
</head>
<body>
    <p><b>ФИО: </b> <?= $FIO ?></p>
    <p><b>Дата рождения: </b><?= $birthday ?></p>
    <p><b>Сумма на счетах: </b><?= $account_sum ?></p>

    <p><b>Номера телефонов:</b></p>
    <div><?php
        foreach ($phone_numbers as $number) {?>
            +<?= $number ?> <br/>
        <?php } ?>
    </div>

    <p><b>Адреса:</b></p>
    <div><?php
        foreach ($addresses as $address) {?>
            <?= $address ?> <br/>
        <?php } ?>
    </div>

    <input id="phone_id" style="display: none" value="<?= $phone_id ?>">
    <script type="text/javascript" src="../../../js/jquery-3.0.0.js"></script>
    <script type="text/javascript" src="../../../js/call.js"></script>
</body>
