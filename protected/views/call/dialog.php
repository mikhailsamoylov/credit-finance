﻿<?php
/**
 * @var array $results
 * @var array $phone_types
 * @var array $address_types
 * @var string $phone_id
 */
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <title>Call to Credit Finance</title>
    <link rel="stylesheet" href="../../../css/dialog.css" type="text/css">
</head>
<body>
    <h4>Уважаемый клиент,</h4>
    <p>мы рады приветствовать вас на горячей линии нашей компании.</p>
    <p>Чем могу вам помочь?</p>
    <p>В мои права входит сообщить вам состояние вашего лицевого счёта, подключить или отключить услуги, а также принять жалобу
    или предложение, касающееся обслуживания клиентов.</p>

    <h4>Результаты:</h4>
    <form id="results" action="/?r=save" method="post">
        <div>
            <label>Результат диалога
                <select id="result" name="result" class="block">
                    <?php
                    foreach ($results as $result) { ?>
                        <option value="<?= $result['id'] ?>"><?= $result['caption'] ?></option>
                    <?php } ?>
                </select>
            </label>
        </div>

        <div>
            <label>Комментарий
                <textarea id="comment" name="comment" class="block"></textarea>
            </label>
        </div>

        <div>
            <label>Новый номер телефона<br/>
                <input id="phone_number" name="phone_number" type="text"/>
                <select id="phone_type" name="phone_type">
                    <?php
                    foreach ($phone_types as $phone_type) { ?>
                        <option value="<?= $phone_type['id'] ?>"><?= $phone_type['caption'] ?></option>
                    <?php } ?>
                </select>
            </label>
        </div>

        <div>
            <label>Новый адрес<br/>
                <input id="address" name="address" type="text"/>
                <select id="address_type" name="address_type">
                    <?php
                    foreach ($address_types as $address_type) { ?>
                        <option value="<?= $address_type['id'] ?>"><?= $address_type['caption'] ?></option>
                    <?php } ?>
                </select>
            </label>
        </div>

        <input id="phone_id" name="phone_id" style="display: none" value="<?= $phone_id ?>"><br/>
        <input type="submit" value="Отправить" style="margin-top: 10px">
    </form>

    <script type="text/javascript" src="../../../js/jquery-3.0.0.js"></script>
    <script type="text/javascript" src="../../../js/dialog.js"></script>
</body>
