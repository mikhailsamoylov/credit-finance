<?php

return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => "Credit-finance",
	'preload' => array('log'),
	'import' => array(
		'application.models.*',
		'application.components.*'
	),
	'modules' => array(),
	'components' => array(
		'db' => require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'database.php'),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),
	'params' => array(
		'baseUrl' => 'credit-finance'
	),
);