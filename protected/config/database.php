<?php

return array(
	'class' => 'system.db.CDbConnection',
	'connectionString' => 'sqlite:protected/data/db.sqlite',
	'emulatePrepare' => true,
	'charset' => 'utf8',
	'enableParamLogging' => true,
);